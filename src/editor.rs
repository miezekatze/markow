use std::io::{Write, stdout, stdin};
use termion::cursor::DetectCursorPos;
use termion::*;
use termion::raw::IntoRawMode;
use termion::event::{Key,Event};
use termion::input::TermRead;

pub struct LineEditor<F, F2>
where F: FnMut(String) -> Option<String>,
      F2: FnMut(String) -> Option<String>{
    string: Vec<char>,
    cursor: usize,
    prompt: &'static str,
    buffer_function: F,
    complete: F2,
    buffer: String,
    history: Vec<Vec<char>>,
    history_idx: usize,
}

impl<F, F2> LineEditor<F, F2>
where F: FnMut(String) -> Option<String>,
      F2: FnMut(String) -> Option<String> {
    pub fn new(prompt: &'static str, buffer_function: F, complete: F2) -> Self {
        LineEditor { string: vec![], cursor: 0, prompt, buffer_function, buffer: "".to_string(),
                     complete,
                     history: vec![],
            history_idx: 0,
        }
    }

    pub fn get_history(&self) -> Vec<Vec<char>> {
        self.history.clone()
    }

    pub fn set_history(&mut self, hist: Vec<Vec<char>>) {
        self.history = hist;
    }

    fn render(&mut self, mut stdout: impl Write, start_pos: &mut (u16, u16)) {
        let input = self.string.iter().collect::<String>();

        let old_buffer = self.buffer.clone();
        self.buffer = (self.buffer_function)(input.clone()).unwrap_or_default();
        let width: usize = termion::terminal_size().unwrap().0.into();
        if self.buffer.len() > width {
            self.buffer.insert_str(width-3, "...");
            for _ in width..self.buffer.len() {
                self.buffer.remove(width);
            }
        }

        // display buffer
        if ! (self.buffer.is_empty() && old_buffer.is_empty()) {
            write!(stdout, "{}\r\n{}{}[90m{}\r{}{}[0m",
                   cursor::Goto(start_pos.0, start_pos.1),
                   clear::CurrentLine,
                   27 as char,               // set color
                   self.buffer,              // buffer
                   cursor::Up(1),            // go up
                   27 as char,
            ).unwrap();
            let nrow = stdout.cursor_pos().unwrap().1;
            start_pos.1 = nrow;
        }

        let len = self.prompt.len() + self.cursor;
        let cpos = (start_pos.1 - 1) * width as u16 + (start_pos.0 - 1) + len as u16;
        // display input
        write!(stdout, "{}{}{}{}{}",
               clear::CurrentLine,       // clear line
               cursor::Goto(start_pos.0, start_pos.1),
               self.prompt,              // show prompt
               input,                    // show input
               cursor::Goto(cpos % width as u16 + 1, cpos / width as u16 + 1)
        ).unwrap();
        
        stdout.flush().unwrap();
    }

    fn remove(&mut self, index: usize) {
        if index < self.string.len() {
            self.string.remove(index);
        }
    }

    /// deletes one word backwards
    fn get_current_word_start(&self) -> usize {
        // if at start, exit
        if self.cursor == 0 {
            return 0;
        }
        let mut word_start: usize = 0;
        let mut i = self.cursor-1;

        // go back until the word starts
        while let Some(' ') = self.string.get(i) {
            i -= 1;
        }

        while i > 0  {
            if let Some(' ') = self.string.get(i) {
                word_start = i+1;
                break;
            }
            i -= 1;
        }

        word_start
    }

    fn get_next_word_start(&self) -> usize {
        let mut word_start: usize = self.cursor;

        // go forwards until the word starts
        while let Some(x) = self.string.get(word_start) {
            if *x == ' ' {
                break;
            }
            word_start += 1;
        }

        if let Some(' ') = self.string.get(word_start) {
            word_start += 1;
        }

        word_start
    }

    fn delete_word(&mut self) {
        if self.cursor == 0 {
            return;
        }
        let word_start = self.get_current_word_start();

        // delete the word
        for _ in word_start..self.cursor {
            self.remove(word_start);
            self.cursor -= 1;
        }
    }

    fn kill_word(&mut self) {
        while let Some(c) = self.string.get(self.cursor) {
            if *c != ' ' { break; }
            self.remove(self.cursor);
        }
        // delete the word
        while let Some(c) = self.string.get(self.cursor)  {
            if *c == ' ' {
                break;
            }
            self.remove(self.cursor);
        }
    }

    fn insert(&mut self, ch: char) {
        self.string.insert(self.cursor, ch);
        self.cursor += 1;
    }

    fn append(&mut self, ch: char) {
        self.string.insert(self.string.len(), ch);
    }

    fn get(&mut self) -> String {
        self.string.iter().collect()
    }
    
    fn clear(&mut self) {
        self.string.clear();
        self.cursor = 0;
    }

    pub fn clear_screen(&mut self) {
        let mut stdout = stdout().into_raw_mode().unwrap();
        write!(stdout, "{}{}", clear::All, cursor::Goto(1, 1)).unwrap();
    }

    fn clear_right(&mut self) {
        self.string = self.string.iter()
            .take(self.cursor)
            .cloned()
            .collect();
    }
    
    fn finish_line(&mut self, mut stdout: impl Write) -> String {
        self.append('\n');
        let result = self.get();
        self.clear();
        write!(stdout, "\r\n").unwrap();
        stdout.flush().unwrap();
        result
    }

    fn left(&mut self) {
        self.cursor = (self.cursor as isize -1).max(0) as usize;
    }

    fn right(&mut self) {
        self.cursor = (self.cursor+1).min(self.string.len());
    }

    fn histidx(&self) -> usize {
        self.history.len() - self.history_idx - 1
    }
    
    /// get user input into Stirng
    pub fn get_line(&mut self) -> Result<String, Option<String>> {
        self.history_idx = 0;
        self.history.retain(|x| ! x.is_empty());
        self.history.push(vec![]);

        // switch to raw mode
        let mut stdout = stdout().into_raw_mode().unwrap();
        let stdin = stdin();

        let mut start_pos = stdout.cursor_pos().unwrap();

        // render current line editor state
        self.render(&mut stdout, &mut start_pos);

        // handle keys -> line editor
        for c in stdin.events() {
            if let Event::Key(key) = c.unwrap() {
                match key {
                    Key::Char('\t') => {
                        let string = self.string.iter().collect::<String>();
                        let w = (self.complete)(string);
                        if let Some(a) = w {
                            self.cursor = self.string.len() - 1;
                            while self.cursor > 0 && self.string[self.cursor] != ' ' {
                                self.cursor -= 1;
                            }
                            if self.cursor > 0 {
                                self.cursor += 1;
                            }

                            self.clear_right();
                            for c in a.chars() {
                                self.append(c);
                            }
                            self.append(' ');
                            self.cursor = self.string.len();
                        }
                    },
                    Key::Ctrl('j') | Key::Ctrl('m') | Key::Char('\n') => {
                        let idx = self.histidx();
                        self.history[idx] = self.string.clone();

                        if self.string.is_empty() || self.string[0] == '/' {
                            self.history.pop();
                        }
                        return Ok(self.finish_line(stdout));
                    },
                    Key::Char(k) => {
                        self.insert(k);
                    },
                    Key::Ctrl('c') => {
                        self.clear();
                        write!(stdout, "\r\n").unwrap();
                        self.history.pop();
                        return Ok("\n".into());
                    },
                    Key::Ctrl('d') => {
                        if self.string.is_empty() {
                            write!(stdout, "\r\n").unwrap();
                            self.history.pop();
                            return Err(None);
                        } else {
                            self.remove(self.cursor);
                        }
                    },
                    Key::Backspace => {
                        self.left();
                        self.remove(self.cursor);
                    },
                    Key::Left | Key::Ctrl('b') => {
                        self.left();
                    },
                    Key::Right | Key::Ctrl('f') => {
                        self.right();
                    },
                    Key::Up | Key::Ctrl('p') => {
                        let nidx = self.history_idx + 1;
                        if nidx < self.history.len() {
                            let idx = self.histidx();
                            self.history[idx] = self.string.clone();
                            self.history_idx = nidx;
                            let idx = self.histidx();

                            self.string = self.history[idx].clone();
                            self.cursor = self.string.len();
                        }
                    },
                    Key::Down | Key::Ctrl('n') => {
                        if self.history_idx > 0 {
                            let nidx = self.history_idx - 1;

                            let idx = self.histidx();
                            self.history[idx] = self.string.clone();
                            self.history_idx = nidx;
                            let idx = self.histidx();

                            self.string = self.history[idx].clone();
                            self.cursor = self.string.len();
                        }
                    },
                    Key::Home | Key::Ctrl('a') => {
                        self.cursor = 0;
                    },
                    Key::End | Key::Ctrl('e') => {
                        self.cursor = self.string.len();
                    },
                    Key::Ctrl('l') => {
                        self.clear_screen();
                        start_pos = (1, 1);
                    },
                    Key::Ctrl('k') => {
                        write!(stdout, "{}", clear::UntilNewline).unwrap();
                        self.clear_right();
                    },
                    Key::Alt('\u{7f}') | Key::Ctrl('h') => {
                        self.delete_word();
                    },
                    Key::Alt('d') => self.kill_word(),
                    Key::Alt('b') => self.cursor = self.get_current_word_start(),
                    Key::Alt('f') => self.cursor = self.get_next_word_start(),
                    _ => {}
                }
            }
            self.render(&mut stdout, &mut start_pos);
        }

        self.history.pop();
        Err(Some("stdin unexpectedly closed...".into()))
    }

}
