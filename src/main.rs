mod editor;

use std::{collections::HashMap, io::Read};

#[derive(Debug)]
pub struct Data<const N: usize> {
    pub freqs: HashMap<[String; N], Vec<(String, usize)>>,
    pub freqs1: HashMap<[String; 1], Vec<(String, usize)>>,
}

fn is_valid_char(c: char) -> bool {
    c.is_alphanumeric()
        || c.is_whitespace()
        || c == '-' || c == '_'
        || c == '.' || c == ','
        || c == '/'
}

fn tokenize(s: String) -> Vec<String> {
    let s = s.replace('.', " . ");
    let s = s.replace(',', " , ");
    let s = s.replace('–', "-"); // wikipedia used weird - sign...
    let ns: String = s.chars()
        .filter(|c| is_valid_char(*c))
        .collect();
    let ns: Vec<String> = ns.split(char::is_whitespace)
        .filter(|x| ! x.is_empty())
        .map(|x| x.to_owned()
            .to_lowercase())
        .collect();

    ns
}

type Randdev<'a> = &'a mut std::fs::File;

impl<const N: usize> Data<N> {
    fn create_map<const M: usize>(convd: &[String]) -> HashMap<[String; M], Vec<(String, usize)>> {
        eprintln!("creating {M}-word map...");
        let map = convd
            .windows(M+1)
            .map(|xs| (xs[0..M]
                       .to_owned()
                       .try_into()
                       .unwrap(), xs[M].clone()))
            .collect::<Vec<([String; M], String)>>();

        let mut m = HashMap::new();
        for (a, b) in map.into_iter() {
            use std::collections::hash_map::Entry;
            match m.entry(a) {
                Entry::Occupied(ref mut v) => {
                    let vals: &mut Vec<(String, usize)> = v.get_mut();
                    let mut found = false;
                    for v in vals.iter_mut() {
                        if v.0 == b {
                            v.1 += 1;
                            found = true;
                            break;
                        }
                    }
                    if ! found {
                        vals.push((b, 1));
                    }
                },
                Entry::Vacant(v) => {
                    v.insert(vec![(b, 1)]);
                },
            }
        }
        m
    }
    
    pub fn new(input: String) -> Self {
        let convd = tokenize(input);

        let m1 = Self::create_map(&convd);
        let m2 = Self::create_map(&convd);
        Self {
            freqs: m1,
            freqs1: m2,
        }
    }

    pub fn predict(&self, s: &[String], dev: Option<Randdev>) -> Option<String> {
        let w = if s.len() >= N {
            let lastn: [String; N] = s.iter()
                .skip(s.len() - N)
                .cloned()
                .collect::<Vec<String>>()
            .try_into()
                .unwrap();
            self.freqs.get(&lastn)?.to_vec()
        } else {
            let last = [s.last().unwrap().clone()];
            let mut vs = self.freqs1.get(&last)?.to_vec();
            // for v in self.freqs1.get(&last)? {
            //         let mut vec = s.to_vec();
            //         vec.push(v.0.clone());

            //         for k in self.freqs.keys() {
            //             if k.starts_with(&vec) {
            //                 vs = vec![v.clone()];
            //             }
            //         }
            // };
            vs
        };

        match dev {
            Some(dev) => Some(choose_random(&w, dev).to_string()),
            None => {
                let mut nw: Vec<_> = w.iter().enumerate().map(|(i, (_, n))| (i, n)).collect();
                nw.sort_by(|a, b| a.1.cmp(b.1).reverse());
                Some(w[nw[0].0].0.to_string())
            },
        }
    }

    pub fn suggest(&self, s: String) -> Option<String> {
        let is_new_word = s.ends_with(' ');
        let mut v = tokenize(s);
        if v.is_empty() {
            return None;
        }

        if is_new_word {
            return Some(self.predict(&v, None)?.to_string());
        }
        
        let s = v.remove(v.len() - 1);

        let mut maxlen = 0;
        let mut maxword = String::new();
        for k in self.freqs1.keys() {
            if k[0].starts_with(&s) {
                let v = &self.freqs1[k];
                let n: usize = v.iter().map(|(_ ,v)| v).sum();

                if n > maxlen {
                    maxlen = n;
                    maxword = k[0].clone();
                }
            }
        }
        if maxlen > 0 {
            Some(maxword)
        } else {
            None
        }
    }
}

fn randlong(dev: Randdev) -> usize {
    let mut buf = [0u8; 8];
    dev.read_exact(&mut buf).unwrap();
    usize::from_le_bytes(buf)
}

fn rand(low: usize, high: usize, dev: Randdev) -> usize {
    ((randlong(dev) as f64 / usize::MAX as f64)
    * (high - low) as f64) as usize + low
}

fn choose_random<'a>(words: &'a [(String, usize)], dev: Randdev) -> &'a str {
    let len: usize = words.iter().map(|(_, b)| b).sum();
    let mut v = rand(0, len, dev);
    for (val, a) in words {
        if v < *a {
            return val;
        }
        v -= *a;
    }
    unreachable!("v [{v}] > len [{len}]")
}

fn main() {
    let mut trainstring = String::new();
    for a in std::fs::read_dir("./train").unwrap() {
        let p = a.unwrap().path();
        if ! p.to_str().unwrap().ends_with(".txt") {
            continue;
        }
        eprintln!("loading {}...", p.to_str().unwrap());
        let train = std::fs::read_to_string(p)
            .unwrap_or_else(|e| {
                eprintln!("Error reading training data: {e}");
                std::process::exit(1);
            });
        trainstring.push_str(&train);
    }

    let data = Data::<3>::new(trainstring);

    let mut randdev = std::fs::File::open("/dev/urandom")
        .unwrap();

    let mut editor = editor::LineEditor::new("> ",
                            |v| data.suggest(v).map(|v| format!("Suggested: {v}")),
                                             |v| data.suggest(v));

    let histfile = ".history";
    if let Ok(v) = std::fs::read_to_string(histfile) {
        editor.set_history(v.lines().map(|v| v.chars().collect()).collect())
    }

    loop {
        let s = match editor.get_line() {
            Ok(a) => a,
            Err(None) => {break;},
            Err(Some(e)) => {
                eprintln!("Error: {e}");
                break;
            },
        };

        let mut text;
        match s.strip_prefix('/').and_then(|x| x.strip_suffix('\n')) {
            Some("help") => {
                println!("<words>  -- generate from starting word[s]");
                println!("/help    -- show this help");
                println!("/quit    -- quit the application");
                println!("/history -- show history");
                println!("/hclear  -- clear history");
                println!("/clear   -- clear the screen");
                println!("/random  -- generate random sequence");
                continue;
            },
            Some("quit") => break,
            Some("history") => {
                let hist = editor.get_history();
                for (i, s) in hist.iter()
                    .map(|l| l.iter().collect::<String>()).enumerate() {
                        println!("{i}: {s}", i = hist.len() - i - 1);
                    }
                continue;
            },
            Some("hclear") => {
                editor.set_history(vec![]);
                continue;
            },
            Some("clear") => {
                editor.clear_screen();
                continue;
            },
            Some("random") => {
                let start = data.freqs.keys().nth(rand(0, data.freqs.len(), &mut randdev))
                    .unwrap();
                text = start.to_vec()
            },
            Some(unknown) => {
                eprintln!("unknown command `{unknown}'");
                continue;
            }
            None => {
                text = tokenize(s);
            },
        }

        if text.is_empty() {
            continue;
        }

        for w in text.iter() {
            print!("{w} ");
        }

        let mut dots = 0;
        let max_dots = 5;
        while let Some(word) = data
            .predict(&text, Some(&mut randdev)) {
                if word == "." {
                    dots += 1;
                    if dots >= max_dots {
                        break;
                    }
                }
                print!("{word} ");
                text.push(word.to_string());
            }
        println!(".");
    }

    let hist = editor.get_history();
    let s = hist.iter()
        .map(|l| l.iter().collect::<String>())
        .collect::<Vec<String>>();
    std::fs::write(histfile, s.join("\n") + "\n").unwrap();
}
